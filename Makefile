BUILD = DOCKER_BUILDKIT=1 docker build
RMI = docker image rm

all: build

build:
	$(BUILD) -t dev:buster --build-arg DISTRO=debian --build-arg RELEASE=buster .
	$(BUILD) -t dev:focal --build-arg DISTRO=ubuntu --build-arg RELEASE=focal .

clean:
	$(RMI) dev:buster
	$(RMI) dev:focal

