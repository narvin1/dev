Docker Dev
==========

A minimal development environment in a docker container with access to the
host docker socket so you can manage host containers, and don't have to
install dev tools, besides docker, on the host.

Installation
------------

Build the development environment images.

```Shell
make
```

Usage
-----

Compose up Debian Buster and Ubuntu Focal dev environments with:
- `sudo`, `zsh`, `git`, `vim`, `make`, and `docker-compose` installed with
    their dependencies
- the time zone set to `America/New_York`
- the terminal set to `xterm-256color`
- a user `doc` with a user ID of `1000`, and `sudo` access without a password
- `bash` and `zsh` configured to source their config files from `~/.config`
- my dotfiles repo cloned into `~/.config`
- `lint` installed from https://gitlab.com/narvin/lint.git
- access to the host docker socket so you can run docker in docker
- a bind mounted host project folder symlinked in the users home directory

```Shell
docker-compose up -d
```

You can exec into either environment.

```Shell
docker exec -it -u doc docker-dev_buster_1 /bin/bash
docker exec -it -u doc docker-dev_focal_1 /bin/bash
```

You can execute `docker` or `docker-compose` command inside of either
environment.

```Shell
docker ps -a
docker run -it myimage
docker-compose up
```

You can build an alternate image with a different user and user ID to match
your identity on the host, this way any files you create in the bind mounted
volume in the container will be owned by your user on the host. You can also
specify the distribution/release (`debain`/`buster` or `ubuntu`/`focal`),
and clone your own dotfiles repo into `~/.config`.

```Shell
DOCKER_BUILDKIT=1 docker build \
    -t dev:me \
    --build-arg USER=me \
    --build-arg UID=1005 \
    --build-arg DISTRO=debian \
    --build-arg RELEASE=buster \
    --build-arg DOTFILES=https://gitlab.com/myrepo/dotfiles \
    .
docker run -it -u me -v /local/path/to/project:/home/me/project dev:me
```

You can also use `git` and `vim`, and access files on the host directory
`~/project`.

