ARG DISTRO=debian
ARG RELEASE=buster
ARG TIMEZONE=America/New_York
ARG USER=doc
ARG UID=1000
ARG DOTFILES=https://gitlab.com/narvin/dotfiles.git

FROM debian:"${RELEASE}" as packages-debian
LABEL maintainer="Narvin Singh <narvin@nmco.us>"
RUN apt-get update \
  && apt-get -y install \
    sudo=1.8.27-1+deb10u3 \
    zsh=5.7.1-1 \
    git=1:2.20.1-2+deb10u3 \
    vim=2:8.1.0875-5 \
    make=4.2.1-1.2 \
    docker-compose=1.21.0-3 \
    pass=1.7.3-2 \
  && rm -rf /var/lib/apt/lists/*

FROM ubuntu:"${RELEASE}" as packages-ubuntu
LABEL maintainer="Narvin Singh <narvin@nmco.us>"
RUN apt-get update \
  && apt-get -y install \
    sudo=1.8.31-1ubuntu1.2 \
    zsh=5.8-3ubuntu1 \
    git=1:2.25.1-1ubuntu3.1 \
    vim=2:8.1.2269-1ubuntu5 \
    make=4.2.1-1.2 \
    docker-compose=1.25.0-1 \
  && rm -rf /var/lib/apt/lists/*

FROM packages-"${DISTRO}" as main
LABEL maintainer="Narvin Singh <narvin@nmco.us>"
ARG DISTRO
ARG RELEASE
ARG TIMEZONE
ARG USER
ARG UID
ARG DOTFILES
ENV POWERLINE=0
ENV PS1_USER="${USER}"
ENV PS1_HOST="${DISTRO}-${RELEASE}"
RUN ln -sf /usr/share/zoneinfo/"${TIMEZONE}" /etc/localtime \
  && useradd -m -u "${UID}" -G sudo -s /bin/bash "${USER}" \
  && printf '%s ALL=(ALL) NOPASSWD:ALL' "${USER}" >> /etc/sudoers \
  && printf 'export TERM=xterm-256color\n' >> /etc/zsh/zshenv \
  && printf 'export ZDOTDIR=${HOME}/.config/zsh\n' >> /etc/zsh/zshenv \
  && printf 'export TERM=xterm-256color\n' >> /etc/bash.bashrc \
  && printf 'source ${HOME}/.config/bash/bashrc\n' >> /etc/bash.bashrc \
USER "${USER}"
WORKDIR /home/"${USER}"
RUN git clone "${DOTFILES}" .config \
  && mkdir src \
  && git clone https://gitlab.com/narvin/lint.git ./src/lint \
  && ( cd ./src/lint && sudo make ) \
  && ln -s /var/project project
ENTRYPOINT ["/bin/bash"]

